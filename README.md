# DevOps

# this an academic project : Getting Started with Git

## Part 1: Setting up the Git Environment

# NB: This project contain two branch

### 1. Generate SSH Key

   a. Use the following command to generate your SSH key:
   
   ```bash
      ssh-keygen -t rsa -b 4096 -C your@email.com
   ```    

   b. Copy your SSH public key using the following command:

   ```bash
      cat ~/.ssh/id_rsa.pub
   ``` 
   c. Log in to your account on the remote repository (e.g., GitHub) and add your SSH public key in your account settings.

### 2. Git Configuration

   Configure your name and email address to identify your commits correctly:
   
   ```bash
   git config --global user.name "Your Name"
   git config --global user.email your@email.com
   ```
### 3. SSH Connection to Remote Repositories 

   ```bash
    # Test your SSH connection to the remote repository (replace with your remote server address)
    ssh -T git@github.com
    # or
    ssh -T git@gitlab.com
   ```
## Part 2: Git Basic Concepts
### 1. Initialize a Local Git Repository

   ```bash
        # Create an empty directory for your project and initialize a Git repository inside it
        mkdir my_project
        cd my_project
        git init
   ```
### 2. Working with Files

   ```bash
    # Create a file called 'index.html' in your project directory and add content
    touch index.html
    echo "Content of your file" > index.html

    # Use the following commands to add the file to the staging area and commit your first changes
    git add index.html
    git commit -m "Initial commit: added index.html"
   ```
### 3. Viewing Commit History

   ```bash
    # View the commit history of your project
    git log

    # To display differences between two commits
    git diff commit_id_1 commit_id_2
   ```
## Part 3: Collaborating with Git
### 1. Branching

   ```bash
    # Create a new branch to work on a specific feature of your project
    git branch my-feature
    git checkout my-feature
   ```
### 2. Working with Remote Repositories

   ```bash
    # Clone a remote repository to your local machine
    git clone git@github.com:your_user/your_repository.git

    # Make changes in your local repository, add them to the index, commit, and push changes to the remote repository
    git add .
    git commit -m "Changes in my-feature"
    git push origin my-feature
   ```

### 3. Managing Conflicts

   ```bash
    # Simulate a conflict by editing the same line in two different branches, then merge the branches and resolve the conflict
    git checkout my-feature
    # Edit a file
    git commit -am "Changes in my-feature"
    git checkout master
    # Edit the same line in the same file
    git commit -am "Changes in master"
    git merge my-feature
    # Resolve the conflict
    git add .
    git commit -m "Conflict resolution"
   ```
## Part 4: Git Flow Usage
### 1. Initializing Git Flow

   ```bash
    # Initialize Git Flow
    git flow init
   ```

### 2. Creating a New Feature

   ```bash
    # Start a new feature
    git flow feature start my-feature
   ```
### 3. Creating a New Release

   ```bash
    # Start a new release
    git flow release start my-release
   ```
### 4. Creating a Hotfix

   ```bash
    # Start a hotfix
    git flow hotfix start my-fix
   ```
